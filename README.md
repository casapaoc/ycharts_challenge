This README will walk you through the steps needed to install Python. It also includes steps on how to run the unit tests included with this hash map implementation.

#### INSTALLATION:
1. Check if you already have Python installed. If you are on Linux, Mac, or any other *nix system, run `python --version` in your terminal. This should print out a version number such as _Python 2.7.12_. Either Python 2 or Python 3 will work. If you already have Python installed, please skip to the **RUNNING TESTS** instructions. If not, continue to the next step.
2. Check out the installation instructions at the official Python website: https://www.python.org/downloads/.

#### RUNNING TESTS:
1. If you have not yet checked out the repo, please run `git clone https://casapaoc@bitbucket.org/casapaoc/ycharts.git`. This will clone the repository into your current working directory.
2. Navigate to the directory above the "app" directory (it's the directory containing this README file).
3. Run `python -m unittest discover` from this directory. This will run the unit tests held in `app/test_hash_map.py` and `app/test_linked_list.py`. The tests should run without errors.
4. You can adjust the number of elements in the linked list and hash map by changing the `_LIST_SIZE` and `_NUM_ELEMENTS` variables in the source code then running step 2 again.
