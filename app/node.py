##
# Represents a node in a linked list.
#
# @author Cyril Casapao
##


class Node:

    ##
    # Initializes a node.
    #
    # @param data       The data to be held in this node
    # @param next_node  The node to be placed after this one
    ##
    def __init__(self, data=None, next_node=None):
        self._data = None
        self._next_node = None

        if data is not None:
            self._data = data
        if next_node is not None:
            self._next_node = next_node

    ##
    # Returns the data held in this node.
    ##
    def get_data(self):
        return self._data

    ##
    # Returns the node immediately after this one
    ##
    def get_next(self):
        return self._next_node

    ##
    # Updates the data held in this node.
    #
    # @param data   The new data to be held in this node
    ##
    def set_data(self, data):
        self._data = data

    ##
    # Updates the "next" pointer of this node.
    #
    # @param data   The node to follow this node
    ##
    def set_next(self, next_node):
        self._next_node = next_node
