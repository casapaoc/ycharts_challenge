from .node import Node
from .linked_list import LinkedList

##
# A custom implementation of a hash map. This implementation uses chaining to deal with
# collisions. However, because the map doubles its size after surpassing a load factor
# of 0.7, this should hopefully prevent too many collisions by increasing the possible
# number of empty buckets for indices to land in.
#
# The chaining is implemented by a custom LinkedList and Node implementation. Each
# bucket holds a LinkedList object. Each Node in the LinkedList holds a tuple as data.
#
# @author Cyril Casapao
##


class HashMap:

    # The initial number of buckets. It should be big enough to avoid
    # too many collisions but not so big that it takes up too much
    # space.
    _INITIAL_CAPACITY = 21

    # If the map surpasses this load factor, it will resize to double
    # the capacity.
    _LOAD_THRESHOLD = 0.7

    ##
    # Creates an empty HashMap instance. An optional capacity can be
    # specified. If it is not specified, it will default to 50.
    #
    # @param capacity (OPTIONAL) The initial size of the map
    ##
    def __init__(self, capacity=None):
        self._num_elements = 0

        capacity_to_use = self._INITIAL_CAPACITY

        if capacity is not None:
            capacity_to_use = capacity
        self._data = [None] * capacity_to_use

    ##
    # Gets the given value from the map if it exists. Returns None if a match was not found.
    #
    # @param key    The key to look for
    # @returns      The value matching the key if it is found, None otherwise
    ##
    def get(self, key):
        index = self._compute_hash(key)
        found_element = self._data[index]
        if found_element is None:
            return None
        else:
            return found_element.get(key)

    ##
    # Adds an element to the map.
    #
    # @param key    The key to add to the map
    # @param value  The value associated to the key
    ##
    def put(self, key, value):
        # Before adding, resize the map if it's full
        if self._get_load_factor() > self._LOAD_THRESHOLD:
            self._increase_capacity()

        index = self._compute_hash(key)

        found_element = self._data[index]
        if found_element is None:
            # Bucket was empty: initialize initialize it with a new linked list.
            new_node = Node((key, value))
            self._data[index] = LinkedList(new_node)
            self._num_elements += 1
        else:
            # Bucket already had something in it.
            new_node = Node((key, value))

            # Only add the data if it's not already there
            if found_element.get(key) is None:
                found_element.append(new_node)
                self._num_elements += 1

    ##
    # Given a key, removes the given key-value pair from the hash map. Returns
    # the removed value if it was found.
    #
    # @param key    The key of the value to remove
    # @returns      The removed value if it exists, None otherwise
    ##
    def remove(self, key):
        index = self._compute_hash(key)

        found_element = self._data[index]
        if found_element is None:
            return None
        else:
            value = found_element.remove(key)
            if found_element.get_size() == 0:
                # If the list is empty after removal, set the hash map entry to None
                self._data[index] = None
            if value is not None:
                # LinkedList.remove() returns None if the key wasn't found. So
                # we need to make sure the key was actually there before we
                # decrement the map size.
                self._num_elements -= 1
            return value

    ##
    # Returns the number of elements in this hash map.
    ##
    def size(self):
        return self._num_elements

    ##
    # Returns the current max capacity of this hash map.
    ##
    def _get_capacity(self):
        return len(self._data)

    ##
    # Helper method. Used to compute the "bucket" the key should be placed in.
    #
    # @param key    The key
    # @return       The computed index
    ##
    def _compute_hash(self, key):
        return hash(key) % self._get_capacity()

    ##
    # Helper method. Returns the current load factor of the map.
    ##
    def _get_load_factor(self):
        return self._num_elements / float(self._get_capacity())

    ##
    # Helper method. Doubles the capacity of the hash map and redistributes
    # the keys.
    ##
    def _increase_capacity(self):
        new_capacity = self._get_capacity() * 2
        new_list = [None] * new_capacity

        for linked_list in self._data:
            if linked_list is None:
                continue

            # Need to rehash all the objects in the list too
            current = linked_list.get_head()
            while current is not None:
                key = current.get_data()[0]
                value = current.get_data()[1]
                new_index = self._compute_hash(key)

                # Still need to check for collisions
                found_element = new_list[new_index]
                if found_element is None:
                    # Bucket was empty: initialize initialize it with a new linked list.
                    new_node = Node((key, value))
                    new_list[new_index] = LinkedList(new_node)
                else:
                    new_node = Node((key, value))
                    # Only add the data if it's not already there
                    if found_element is None:
                        found_element.append(new_node)
                current = current.get_next()
        self._data = new_list
