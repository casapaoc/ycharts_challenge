from .node import Node

##
# Represents a linked list. This implementation is meant to be used for chaining
# within a hash map. It should be built of Node objects (see node.py) whose data
# are tuples.
#
# @author Cyril Casapao
##


class LinkedList:

    ##
    # Initializes a new linked list. An optional head Node can be specified. If
    # not specified, head will be initially set to None.
    #
    # @param head (OPTIONAL) The first node in the list
    ##
    def __init__(self, head=None):
        self._head = None
        self._tail = None
        self._size = 0

        if head is not None:
            self._head = head
            self._tail = head
            self._size = 1

    ##
    # Traverses the list to find the given element. The comparison is based on the data
    # stored in the Node.
    #
    # @param target     The key of the data to find in this list
    # @return           The Node element if found, None otherwise
    ##
    def get(self, target):
        current = self.get_head()
        while current is not None:
            current_key = current.get_data()[0]
            if current_key == target:
                return current
            else:
                current = current.get_next()
        return None

    ##
    # Appends an element to the end of the list.
    #
    # @param node   The Node object to append
    ##
    def append(self, node):
        if self.get_size() == 0:
            self._set_head(node)
        else:
            old_tail = self.get_tail()
            old_tail.set_next(node)

        node.set_next(None)
        self._set_tail(node)
        self._size += 1

    ##
    # Looks for the given data in the list and removes and returns it.
    #
    # @param target The key of the data to find
    # @return       The Node if found, None otherwise
    ##
    def remove(self, target):
        if self.get_size() == 0 or self.get_head() is None:
            # Empty list
            return None
        if self.get_size() == 1:
            current_key = self.get_head().get_data()[0]
            if current_key == target:
                # Match found in one element list
                found_node = self.get_head()
                self._set_head(None)
                self._set_tail(None)
                self._size -= 1
                return found_node
            else:
                # No in one element list
                return None

        # A list with more than one element
        prev_node = None
        current = self.get_head()
        while current is not None:
            next_node = current.get_next()
            current_key = current.get_data()[0]
            if current_key == target:
                if current == self.get_head():
                    # Special case 1: Removing head
                    self._set_head(next_node)
                    self._size -= 1
                    return current
                elif current == self.get_tail():
                    # Special case 2: Removing tail
                    prev_node.set_next(None)
                    self._size -= 1
                    self._set_tail(prev_node)
                    return current

                prev_node.set_next(next_node)
                self._size -= 1
                return current
            else:
                # No match; check the next node
                prev_node = current
                current = next_node

        # Not found
        return None

    ##
    # Returns the first element of this list.
    ##
    def get_head(self):
        return self._head

    ##
    # Returns the last (non-None) element in the list.
    ##
    def get_tail(self):
        return self._tail

    ##
    # Returns the number of elements currently in this list.
    ##
    def get_size(self):
        return self._size

    ##
    # Updates the head pointer of this list.
    #
    # @param node   The Node object representing the new head.
    ##
    def _set_head(self, node):
        self._head = node

    ##
    # Updates the tail pointer of this list. This should only
    # be called during an append() operation.
    #
    # @param node   The Node object representing the new tail.
    ##
    def _set_tail(self, node):
        self._tail = node
