import unittest

from .node import Node
from .linked_list import LinkedList


class LinkedListTest(unittest.TestCase):

    _LIST_SIZE = 2000

    def setUp(self):
        self._linked_list = LinkedList()

    def test_add(self):
        for key in range(0, self._LIST_SIZE):
            value = self._int_to_char(key)
            new_node = Node((key, value))

            old_size = self._linked_list.get_size()
            self._linked_list.append(new_node)
            new_size = self._linked_list.get_size()

            self.assertEqual(old_size + 1, new_size)

    def test_successful_get(self):
        self._initialize_list(self._LIST_SIZE)
        for key in range(0, self._LIST_SIZE):
            calculated_value = self._int_to_char(key)
            found_node = self._linked_list.get(key)

            self.assertTrue(found_node is not None)
            self.assertEqual(key, found_node.get_data()[0])
            self.assertEqual(calculated_value, found_node.get_data()[1])

    def test_successful_remove(self):
        self._initialize_list(self._LIST_SIZE)
        for key in range(0, self._LIST_SIZE):
            calculated_value = self._int_to_char(key)

            old_size = self._linked_list.get_size()
            found_node = self._linked_list.remove(key)
            new_size = self._linked_list.get_size()

            self.assertTrue(found_node is not None)
            self.assertEqual(key, found_node.get_data()[0])
            self.assertEqual(calculated_value, found_node.get_data()[1])
            self.assertEqual(old_size - 1, new_size)

    ##
    # Helper method to initialize a list of a given size.
    ##
    def _initialize_list(self, size):
        for key in range(0, size):
            value = self._int_to_char(key)
            new_node = Node((key, value))
            self._linked_list.append(new_node)

    ##
    # Helper method. Converts an integer to an uppercase
    # alphabetical letter (ASCII codes 65 to 90 inclusive).
    ##
    def _int_to_char(self, x):
        return chr((x % 26) + 65)


def main():
    unittest.main()


if __name__ == '__main__':
    main()
