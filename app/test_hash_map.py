import unittest

from .hash_map import HashMap


class HashMapTest(unittest.TestCase):

    _NUM_ELEMENTS = 50000

    def setUp(self):
        self._map = HashMap()

    ##
    # Tests adding new key-value pairs to the map.
    ##
    def test_put(self):
        for key in range(0, self._NUM_ELEMENTS):
            value = self._int_to_char(key)

            old_size = self._map.size()
            self._map.put(key, value)
            new_size = self._map.size()

            self.assertEqual(old_size + 1, new_size)

    ##
    # Tests getting a key that should be in the map.
    ##
    def test_successful_get(self):
        self._initialize_map(self._NUM_ELEMENTS)
        for key in range(0, self._NUM_ELEMENTS):
            calculated_value = self._int_to_char(key)
            found_element = self._map.get(key)

            self.assertTrue(found_element is not None)
            self.assertEqual(key, found_element.get_data()[0])
            self.assertEqual(calculated_value, found_element.get_data()[1])

    ##
    # Tests getting a key that does not exist in the map.
    ##
    def test_failed_get(self):
        self._initialize_map(self._NUM_ELEMENTS)

        # All these keys should be out of range
        for key in range(self._NUM_ELEMENTS, self._NUM_ELEMENTS * 2):
            found_element = self._map.get(key)
            self.assertTrue(found_element is None)

    ##
    # Tests removing a key that is in the map.
    ##
    def test_successful_remove(self):
        self._initialize_map(self._NUM_ELEMENTS)
        for key in range(0, self._NUM_ELEMENTS):
            calculated_value = self._int_to_char(key)

            old_size = self._map.size()
            found_element = self._map.remove(key)
            new_size = self._map.size()

            self.assertTrue(found_element is not None)
            self.assertEqual(key, found_element.get_data()[0])
            self.assertEqual(calculated_value, found_element.get_data()[1])
            self.assertEqual(old_size - 1, new_size)

    ##
    # Tests removing a key that is not in the map.
    ##
    def test_failed_remove(self):
        self._initialize_map(self._NUM_ELEMENTS)
        for key in range(self._NUM_ELEMENTS, self._NUM_ELEMENTS * 2):
            old_size = self._map.size()
            found_element = self._map.remove(key)
            new_size = self._map.size()

            self.assertTrue(found_element is None)
            self.assertEqual(old_size, new_size)

    ##
    # Helper method to initialize a list of a given size.
    ##
    def _initialize_map(self, size):
        for key in range(0, size):
            value = self._int_to_char(key)
            self._map.put(key, value)

    ##
    # Helper method. Converts an integer to an uppercase
    # alphabetical letter (ASCII codes 65 to 90 inclusive).
    ##
    def _int_to_char(self, x):
        return chr((x % 26) + 65)


def main():
    unittest.main()


if __name__ == '__main__':
    main()